# BM++ (Bookmarks Plus Plus)

I created this extension in a few hours to solve the problem of managing my bookmarks (more than 500), and i thought i'd share it with the world, that's it!

It would've taken me days if it wasn't for the following awesome componenets i've used:

*   Yeoman! (chrome extension generator).
*   AngularJS.
*   UnderscoreJS.
*   angular-underscore (https://github.com/floydsoft/angular-underscore).
*   jQuery.
*   Twitter Bootstrap.
*   StackOverflow (Of course!)

If you liked this extension and would like to improve it feel free to [fork it on bitbucket](https://bitbucket.org/ramigb/bm)*!

You can [e-mail](mailto:rami.g.b@gmail.com) me or [follow me](http://www.twitter.com/ramigb) on twitter as well.

You are awesome if you are still reading this.

You know what? you can also listen to [my music](http://www.soundcloud.com/ramigb) since you are so cool and so bored lol.

### License

This extension is released under MIT license:

      The MIT License (MIT)
      Copyright (c) 2014 Rami Gb
      Permission is hereby granted, free of charge, to any person obtaining a copy
      of this software and associated documentation files (the "Software"), to deal
      in the Software without restriction, including without limitation the rights
      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
      copies of the Software, and to permit persons to whom the Software is
      furnished to do so, subject to the following conditions:
      The above copyright notice and this permission notice shall be included in all
      copies or substantial portions of the Software.
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      SOFTWARE.

* * *

      *Bitbucket is what i use for my production, i love the free repos and the ease of use.